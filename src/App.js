import React, { Component } from "react";
import "./App.css";
import Person from './Person/Person';
import PersonClass from './Person/ClassPerson';


class App extends Component {
  render() {
    // return (
    //   <div className="App">
    //    <h1> hello, i am a react app </h1>
    //   </div>
    // ); -> this code complies to what we have written below
    //createElement takes infinite amounts of arguments
    //the first argument is what -> we want to render to the dom - a normal html element like div, your own component
    //the second arugment -> configuration for this
    // third: any amount of children -> element that goes inside of div and other elemetns
    // const reactEl_1 = React.createElement('div', null, 'h1', 'Hello!...I am a react Application!!!');
    // const reactEl_2 = React.createElement('div', null, React.createElement('h1', {className: 'App'}, 'This is second element, and h1 tag SHOULD work'));

    return (
      <div className="App">
        <h2> hello bs</h2>
        <Person name="Ahmed">
          <h2>I am a child h4 compoent inside the Person tag</h2>
        </Person>
        <Person name="Jon doe">
          <h2>I am another child element</h2>
        </Person>
        <Person name="Kassas City"><h2>Me too!</h2></Person>
        <Person name="Georgia State"></Person>

        <PersonClass name2="Ibrahim dialo">
          <a href="www.google.com">Google dot com is a child href element</a>
        </PersonClass>
      </div>
    );
  }
}

export default App;
