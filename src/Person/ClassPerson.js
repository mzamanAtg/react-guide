import React from 'react';

export default class ClassPerson extends React.Component {
    render (){
        return (
            <div>
                <h2>Hello, I am a <u>{this.props.name2}</u> Class Component in which I extends the React.Component Class</h2>
                <p>{this.props.children}</p>
            </div>
            
        );
    }
}